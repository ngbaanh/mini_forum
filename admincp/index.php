<!DOCTYPE html>
<html lang="en">
	<!--Phần Head chứa css và định dạng-->
  <?php
  require('header.php');
  ?>
	
	<!--Phần chính phía bên phải-->
	<main role="main">
	<!--Section đầu tiên, lời chào-->
	 <section class="panel important">
		<h2>Xin chào đến với trang quản lý của ban quản trị </h2>
      <ul>
       <li>Chúc bạn một ngày tốt lành.</li>
      </ul>
	 </section>
	 <!--Section thứ 2, thông tin về tổng số tin rao-->
	<section class="panel">
    <h2>Tổng số Bài đăng</h2>
    <ul>
      <li><b>25 </b>Bài đăng</li>
      <li><b>5</b> Bài chưa duyệt.</li> 
    </ul>
	</section>
	</main>
	<footer role="contentinfo">Diễn đàn mini - Bá Anh - Doãn Chánh</footer>
       
    </body>
<!-- Out Body -->
</html>